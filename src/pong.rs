use amethyst::assets::{AssetStorage, Loader};
use amethyst::core::transform::Transform;
use amethyst::ecs::prelude::{Component, DenseVecStorage};
use amethyst::prelude::*;
use amethyst::renderer::{
    Flipped, Camera, PngFormat, Projection, SpriteRender, SpriteSheet,
    SpriteSheetFormat, SpriteSheetHandle, Texture, TextureMetadata, Transparent,
};

//Game struct
pub struct Pong;
impl SimpleState for Pong {
    fn on_start(&mut self, data: StateData<'_, GameData<'_,'_>>){
        let world = data.world;

        let sprite_sheet_handle = load_sprite_sheet(world);

        world.register::<Paddle>();

        initialise_paddles(world, sprite_sheet_handle);
        initialise_camera(world);
    }
}

//define Arena size
pub const ARENA_HEIGHT: f32 = 1080.0;
pub const ARENA_WIDTH: f32 = 1920.0;

//private function for building a camera and initiazling it in a World
fn initialise_camera(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_z(1.0);
    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            ARENA_WIDTH,
            0.0,
            ARENA_HEIGHT,
        )))
        .with(transform)
        .build();
}

//Paddles
const PADDLE_HEIGHT: f32 = (ARENA_HEIGHT / 10.0) * 28.9;
const PADDLE_WIDTH: f32 = (ARENA_WIDTH / 10.0) * 19.1;

fn load_sprite_sheet(world: &mut World) -> SpriteSheetHandle {
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage =
            world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "texture/crab.png",
            PngFormat,
            TextureMetadata::srgb_scale(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store =
        world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "texture/crab.ron",
        SpriteSheetFormat,
        texture_handle,
        (),
        &sprite_sheet_store,
    )
}

fn initialise_paddles(world: &mut World,
                      sprite_sheet: SpriteSheetHandle){
    let mut left_transform = Transform::default();
    let mut right_transform = Transform::default();

    //Position the paddles
    let y = ARENA_HEIGHT / 2.0;
    left_transform.set_xyz(PADDLE_WIDTH * 0.5, y, 0.0);
    right_transform.set_xyz(ARENA_WIDTH - (PADDLE_WIDTH * 0.5), y, 0.0);

    //render the paddles
    let sprite_render = SpriteRender {
        sprite_sheet: sprite_sheet.clone(),
        sprite_number: 0,
    };

    //Create left Paddle
    world
        .create_entity()
        .with(sprite_render.clone())
        .with(Paddle::new(Side::Left))
        .with(left_transform)
        .build();

    //Create right Paddle
    world
        .create_entity()
        .with(sprite_render.clone())
        .with(Transparent)
        .with(Flipped::Horizontal)
        .with(Paddle::new(Side::Right))
        .with(right_transform)
        .build();
}

pub enum Side {
    Left,
    Right,
}

pub struct Paddle {
    pub side: Side,
    pub width: f32,
    pub height: f32,
}

impl Paddle {
    fn new (side: Side) -> Paddle {
        Paddle {
            side,
            width: 1.0,
            height: 1.0,
        }
    }
}

impl Component for Paddle {
    type Storage = DenseVecStorage<Self>;
}
