extern crate amethyst;
mod pong;
use crate::pong::Pong;
use amethyst::{
    prelude::*,
    renderer::{ColorMask, Transparent, DisplayConfig, DrawFlat2D, Pipeline, RenderBundle, Stage, ALPHA},
    core::transform::TransformBundle,
    utils::application_root_dir,
};


fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let path = format!(
        "{}/resources/display_config.ron",
        application_root_dir()
    );
    let config = DisplayConfig::load(&path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.6, 0.6, 0.4, 1.0], 1.0)
            .with_pass(DrawFlat2D::new()
            .with_transparency(ColorMask::all(), ALPHA, None)),
    );

    let game_data =
        GameDataBuilder::default()
        .with_bundle(RenderBundle::new(pipe, Some(config))
                     .with_sprite_sheet_processor()
        )?
        .with_bundle(TransformBundle::new())?;


    let mut game = Application::new("./", Pong, game_data)?;

    game.run();

    Ok(())
}
